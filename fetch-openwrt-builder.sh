#!/bin/sh

set -e

RELEASE=18.06.1
TARGET=ar71xx
TYPE=generic

DL_LINK="https://downloads.lede-project.org/releases/${RELEASE}/targets/${TARGET}/${TYPE}/openwrt-imagebuilder-${RELEASE}-${TARGET}-${TYPE}.Linux-x86_64.tar.xz"

if [ ! -d "lede" ]; then
  wget -qO- ${DL_LINK} | tar xvJ
  mv ./openwrt-imagebuilder-${RELEASE}-${TARGET}-${TYPE}.Linux-x86_64 ./lede
fi